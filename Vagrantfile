# coding: utf-8
# I borrowed some of this file from 
# https://github.com/ecomm-integration-ballerina/kubernetes-cluster/blob/master/Vagrantfile

servers = [
  {
    :name => "master",
    :type => "master",
    :box => "ubuntu/bionic64",
    :box_version =>"20200605.0.0" ,
    :eth1 => "192.168.205.10",
    :mem => "2048",
    :cpu => "2"
  },
  {
    :name => "node-1",
    :type => "node",
    :box => "ubuntu/bionic64",
    :box_version =>"20200605.0.0" ,
    :eth1 => "192.168.205.11",
    :mem => "2048",
    :cpu => "2"
  },
  {
    :name => "node-2",
    :type => "node",
    :box => "ubuntu/bionic64",
    :box_version =>"20200605.0.0" ,
    :eth1 => "192.168.205.12",
    :mem => "2048",
    :cpu => "2"
  }
]

$configurationScript = <<-SCRIPT
  # https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#letting-iptables-see-bridged-traffic
  cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
  net.bridge.bridge-nf-call-ip6tables = 1
  net.bridge.bridge-nf-call-iptables = 1
EOF
  sudo sysctl --system

  # https://kubernetes.io/docs/setup/production-environment/container-runtimes/#docker
  # (Install Docker CE)
  ## Set up the repository:
  ### Install packages to allow apt to use a repository over HTTPS
  apt-get update && apt-get install -y \
  apt-transport-https ca-certificates curl software-properties-common gnupg2

  # Add Docker’s official GPG key:
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

  # Add the Docker apt repository:
  add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

  # Install Docker CE
  apt-get update && apt-get install -y \
    containerd.io=1.2.13-1 \
    docker-ce=5:19.03.8~3-0~ubuntu-$(lsb_release -cs) \
    docker-ce-cli=5:19.03.8~3-0~ubuntu-$(lsb_release -cs)

  # Set up the Docker daemon
  cat > /etc/docker/daemon.json <<EOF
  {
    "exec-opts": ["native.cgroupdriver=systemd"],
    "log-driver": "json-file",
    "log-opts": {
      "max-size": "100m"
    },
    "storage-driver": "overlay2"
  }
EOF

  mkdir -p /etc/systemd/system/docker.service.d

  # Restart Docker
  systemctl daemon-reload
  systemctl restart docker

  # https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-kubeadm-kubelet-and-kubectl
  sudo apt-get update && sudo apt-get install -y apt-transport-https curl
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
  cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
  deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
  sudo apt-get update
  sudo apt-get install -y kubelet kubeadm kubectl
  sudo apt-mark hold kubelet kubeadm kubectl

  # Remaining lines borrowed from https://github.com/ecomm-integration-ballerina/kubernetes-cluster/blob/master/Vagrantfile
  # kubelet requires swap off
  swapoff -a

  # keep swap off after reboot
  sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

  # ip of this box
  IP_ADDR=ip -4 addr show enp0s8 | grep -oP "(?<=inet ).*(?=/)"``

  # set node-ip
  sudo echo "KUBELET_EXTRA_ARGS=--node-ip=$IP_ADDR" > /etc/default/kubelet
  sudo systemctl restart kubelet
SCRIPT

$masterConfiguration = <<-SCRIPT
  echo "This is master"

  # ip of this box
  IP_ADDR=ip -4 addr show enp0s8 | grep -oP "(?<=inet ).*(?=/)"``

  # install k8s master
  HOST_NAME=$(hostname -s)
  kubeadm init --apiserver-advertise-address=$IP_ADDR --apiserver-cert-extra-sans=$IP_ADDR  --node-name $HOST_NAME --pod-network-cidr=172.16.0.0/16

  #copying credentials to regular user - vagrant
  sudo --user=vagrant mkdir -p /home/vagrant/.kube
  cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
  chown $(id -u vagrant):$(id -g vagrant) /home/vagrant/.kube/config

  # install Calico pod network addon
  export KUBECONFIG=/etc/kubernetes/admin.conf
  kubectl apply -f https://docs.projectcalico.org/v3.14/manifests/calico.yaml

  # Create Join Script for Nodes to copy over
  kubeadm token create --print-join-command >> /etc/kubeadm_join_cmd.sh
  chmod +x /etc/kubeadm_join_cmd.sh

  # required for setting up password less ssh between guest VMs
  sudo sed -i "/^[^#]*PasswordAuthentication[[:space:]]no/c\PasswordAuthentication yes" /etc/ssh/sshd_config
  sudo service sshd restart
SCRIPT

$nodeConfiguration = <<-SCRIPT
  echo "This is worker"
  apt-get install -y sshpass
  sshpass -p "vagrant" scp -o StrictHostKeyChecking=no vagrant@192.168.205.10:/etc/kubeadm_join_cmd.sh .
  sh ./kubeadm_join_cmd.sh
SCRIPT

Vagrant.configure("2") do |config|

  servers.each do |opts|
    config.vm.define opts[:name] do |iconfig|

      iconfig.vm.box = opts[:box]
      iconfig.vm.box_version = opts[:box_version]
      iconfig.vm.hostname = opts[:name]
      iconfig.vm.network :private_network, ip: opts[:eth1]

      iconfig.vm.provider "virtualbox" do |v|

        v.name = opts[:name]
        v.customize ["modifyvm", :id, "--groups", "/Cluster"]
        v.customize ["modifyvm", :id, "--memory", opts[:mem]]
        v.customize ["modifyvm", :id, "--cpus", opts[:cpu]]

      end

      iconfig.vm.provision "shell", inline: $configurationScript

      if opts[:type] == "master"
        iconfig.vm.provision "shell", inline: $masterConfiguration
      else
        iconfig.vm.provision "shell", inline: $nodeConfiguration
      end
    end
  end
end
