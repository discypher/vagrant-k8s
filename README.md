# Vagrant Kubernetes Cluster
This is for spinning up a Cluster locally with:  
- 1 x master
- 2 x nodes
- Calico installed

## To run
Have Virtualbox installed, then:  
```
$ cd vagrant-k8s
$ vagrant up
```
